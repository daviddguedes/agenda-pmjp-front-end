# README #

Front end do sistema de divulgação da agenda dos gestores públicos.

### Tecnologias utilizadas ###

* Angular JS
* NODE
* Bower
* [Saiba mais sobre transparência pública](http://transparencia.joaopessoa.pb.gov.br)

### Como instalar? ###

* Intale o NODE na sua máquina
* cd /local/do/projeto
* Rode o comando npm install
* Rode o comando node server.js

### Quem desenvolve este projeto ###

* Equipe de desenvolvimento da secretaria de transparência pública de João Pessoa.